FROM python:3.7

RUN mkdir -p /app /data

WORKDIR /app

COPY . .

RUN pip install --upgrade -r requirements.txt

ENTRYPOINT python bot.py
