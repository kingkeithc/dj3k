"""Test the config module."""
import pytest

import dj3k


class TestConfig:
    """Tests for the Config class."""

    def test_uniqueness(self):
        """Test instances of the Config object for having different identities but the same state."""
        config1 = dj3k.Config()
        config2 = dj3k.Config()

        assert config1 is not config2
        assert config1.__dict__ is config2.__dict__

    def test_initial_state(self):
        """Ensure the objects are initialized empty."""
        config1 = dj3k.Config()
        config2 = dj3k.Config()

        # These should raise exceptions, because the opts attribute should not be set yet.
        with pytest.raises(AttributeError):
            config1.opts['this key'] = 'this thing'
            config2.opts['that key'] = 'that thing'

    def test_shared_state(self):
        """Ensure the state is being shared."""
        config1 = dj3k.Config()
        config2 = dj3k.Config()

        config1.reload()

        assert config1.opts == config2.opts

        config3 = dj3k.Config()
        config3.opts = "its been changed"

        assert config1.opts == config2.opts == config3.opts == "its been changed"
