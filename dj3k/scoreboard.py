"""Defines objects rerlated to the scorekeeping system."""
import logging
import sqlite3


class Scoreboard():
    """Defines a scpreboard."""

    def __init__(self, djbot):
        """Initialize the object."""
        self._log = logging.getLogger('bot.dj3k.Scoreboard')
        self.config = djbot.config

        # Connect to the database, and get a cursor.
        self.db = sqlite3.connect(self.config.dataDir + '/scoreboard.db')
        self.cursor = self.db.cursor()

        # Create score table if is doesn't alreadyt exist
        self.cursor.execute("CREATE TABLE IF NOT EXISTS scores "
                            "(playerID INTEGER NOT NULL PRIMARY KEY, score INTEGER NOT NULL)")

    def __del__(self):
        """Tasks to complete before object is destroyed."""
        self.db.commit()
        self.db.close()

    def setScore(self, playerID, newScore):
        """Set the score for a player to a new value."""
        self.cursor.execute('INSERT OR REPLACE INTO scores '
                            '(playerID, score) VALUES (?, ?)', (playerID, newScore))
        self.db.commit()

    def incrementScore(self, playerID, delta=1):
        """Change the score for a player by a certain amount."""
        newScore = self.getScore(playerID)
        newScore += delta

        self.setScore(playerID, newScore)
        return newScore

    def getScore(self, playerID):
        """Get the score for a player."""
        self.cursor.execute('SELECT score FROM scores WHERE playerID=?', (playerID, ))
        score = self.cursor.fetchone()

        # If there is no score...
        if score is None:
            # ...assume it is 0, and set it to 0 for the player.
            score = 0
            self.setScore(playerID, score)
        else:
            # ...otherwise, get the returned score.
            score = score[0]

        self._log.info(f"Got Score: {score}, for player: {playerID}")
        return score
