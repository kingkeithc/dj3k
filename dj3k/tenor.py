"""Defines objects for working with the Tenor API."""

import random
import logging
import requests


class TenorException(Exception):
    """Thrown by the Tenor class for unrecoverable errors."""

    pass


class Tenor():
    """Object for working with the Tenor API.

    https://tenor.com/gifapi/documentation
    """

    API_AUTHORITY = 'https://api.tenor.com'

    def __init__(self, apiKey):
        """Initialize the object."""
        self._log = logging.getLogger('bot.dj3k.Tenor')
        self.apiKey = apiKey

    def _apiGetRequest(self, endpoint, parameters={}):
        """Make a single request to the Tenor API, verifies and returns a dict response."""
        url = Tenor.API_AUTHORITY + endpoint
        parameters['key'] = self.apiKey

        # Attempt the request
        try:
            self._log.info(f'Making GET request to URL ({url}) with parameters ({parameters}).')
            r = requests.get(url, params=parameters)
        except requests.ConnectionError:
            # If it cannot connect, log an error and move on.
            self._log.error(f'Error connecting to ({url})!')
            return None
        except requests.ConnectTimeout:
            # If it times out.
            self._log.error(f'Timeout connecting to ({url})!')
            return None

        if not r.ok:
            # TODO Add way of hendling retries.
            self._log.error(f'Response from Tenor is not OK! ({r.status_code} {r.reason})')
            return None

        responseJson = r.json()

        if 'error' in responseJson:
            self._log.error(f"Tenor request returned this error: "
                            f"{responseJson['code']} {responseJson['error']}")
            raise TenorException('Response form Tenor API is an error!')
        else:
            self._log.debug(f'Successfull response form Tenor API!\n{responseJson}')
            return responseJson

    def search(self, searchTerm):
        """Return a list of gifs returned by searching the term `searchTerm`."""
        results = self._apiGetRequest('/v1/search', parameters={'q': searchTerm})
        return results['results']

    def searchOne(self, searchTerm):
        """Return the first gif returned by a search."""
        return self.search(searchTerm)[0]

    def randomGifSearch(self, searchTerm):
        """Return a dict representing a random gif returned by the search term `searchTerm`."""
        gifs = self.search(searchTerm)
        return random.choice(gifs)
