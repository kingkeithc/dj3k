"""Defines Borg Class."""


class Borg:
    """Children of this class share the same state."""

    # Variable where shared state is kept
    _shared_state = {}

    def __init__(self):
        """Set the satate of the initialized object to the shared state."""
        self.__dict__ = self._shared_state
