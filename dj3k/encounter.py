"""Module that defines encounters."""
import logging
import asyncio
import random


class Encounter():
    """Logical class of the encounter."""

    def __init__(self, djbot, channel):
        """Initialize the encounter."""
        self._log = logging.getLogger('bot.dj3k.Encounter')
        self.config = djbot.config
        self.channel = channel
        self.ended = False
        self.caught = False
        self.messages = []
        self.scoreboard = djbot.scoreboard

        self.duration = random.randint(
            self.config.encounter_times['duration']['min'],
            self.config.encounter_times['duration']['max']
        )

        self.animal = random.choice(self.config.animals)

    async def start(self):
        """Begin the encounter."""
        self._log.debug(f"Starting an encounter with a {self.animal} in "
                        f"{self.channel.name} lasting {self.duration} seconds.")

        # Send the start message
        self.messages.append(await self.channel.send(
            content=f"A wild {self.animal} has appeared! Type '!d catch' to catch it."))

        # Sleep for the length of the encounter.
        await asyncio.sleep(self.duration)

        # End the encounter.
        await self.end()

    async def end(self):
        """End the encounter if it has not ended already."""
        # If its already over, do nothing.
        if self.ended:
            return

        # If it hasn't already ended, say its ended.
        self.ended = True
        self._log.debug(f"Ending an encounter in {self.channel.guild.name}/{self.channel.name}.")

        # Send a message saying the animal fled if the encounter is ending,
        # but the animal was not caught.
        if not self.caught:
            self.messages.append(await self.channel.send(
                content=f"Uh oh! The {self.animal} fled. Better luck next time."))

        # Wait for a little bit, then delete all messages that were sent as part of this encounter.
        # Just to prevent spam.
        await asyncio.sleep(10)
        for msg in self.messages:
            await msg.delete()

    async def attemptCatch(self, msg):
        """Attempt to catch the animal."""
        # If the encounter is still running, and the animal has not been caught.
        if not self.ended and not self.caught:
            self._log.info(f'{msg.author.name} is going to catch the {self.animal}.')
            # The animal was caught
            self.caught = True
            # Increment the player's score, and send a congrats message.
            score = self.scoreboard.incrementScore(msg.author.id)
            await self.channel.send(content=f"Congratulations {msg.author.mention}! "
                                    f"You caught a {self.animal}. Your score is now {score}.")

            # End the encounter.
            await self.end()
        elif self.ended:
            self._log.info(f'{msg.author.name} Tried to catch, but the encounter is over :|')

