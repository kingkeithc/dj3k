"""Defines configuration related things."""
import os
import sys
import logging
import argparse
import yaml

import dj3k
import dj3k.utils


class Config(dj3k.utils.Borg):
    """Define structure of the config."""

    def __init__(self):
        """Initialize the object."""
        # Initialize the parent class
        super().__init__()

    def reload(self):
        """Refresh the configuration."""
        # Get a logger for this object.
        self._log = logging.getLogger('bot.dj3k.Config')

        # Get a dict of the command line arguments.
        cmdArgs = self.getArgs()

        # Load the config from the passed config file.
        fileConf = self.loadYaml(cmdArgs['configFile'])
        print(f"Loading config from file {cmdArgs['configFile']}.")

        # Load the config from environment variables.
        envConf = self.loadEnv()

        # Merge the three different configs into one final config.
        finalConfig = {}
        finalConfig.update(fileConf)
        finalConfig.update(envConf)
        finalConfig.update(cmdArgs)

        # Pring debug info about the passed arguments.
        self._log.debug(f'Final config parsed into: {finalConfig}')

        # Set this object's opts to a namespace from the finalConfig.
        self.opts = argparse.Namespace(**finalConfig)

    def loadEnv(self):
        """Return dict of config options taken from the environment.

        Anything defined here overrides file variables.
        """
        self._log.debug('Parsing environment variables...')
        envs = {}

        envs['discordToken']        = os.environ.get('DJ_DISCORD_TOKEN', None)
        envs['redditClientID']      = os.environ.get('DJ_REDDIT_CLIENT_ID', None)
        envs['redditClientSecret']   = os.environ.get('DJ_REDDIT_CLIENT_SECRET', None)
        envs['logLevel']            = os.environ.get('DJ_LOG_LEVEL', 20)
        envs['logDir']              = os.environ.get('DJ_LOG_DIR', 'logs/')
        envs['dataDir']             = os.environ.get('DJ_DATA_DIR', 'data/')
        envs['environment']         = os.environ.get('DJ_ENVIRONMENT', 'Unspecified')
        envs['encounterChannel']    = os.environ.get('DJ_ENCOUNTER_CHANNEL', None)
        envs['presence']            = os.environ.get('DJ_PRESENCE', 'Just hanging out')
        envs['prefix']              = os.environ.get('DJ_CMD_PREFIX', '!d')
        envs['tenorApiKey']         = os.environ.get('DJ_TENOR_API_KEY', None)

        # Filter out None keys.
        parsed = {k: v for k, v in envs.items() if v is not None}

        self._log.debug(f'Parsed environment variables: {parsed}')
        return parsed

    def getArgs(self):
        """Parse and return a dict of the command line arguments to this script.

        Anything defined here overrides environment variables and file variables."""
        self._log.debug('Parsing command line arguments...')

        parser = argparse.ArgumentParser()

        # Specify the config file, this is the one argument that cannot be set by an environment variable.
        parser.add_argument('--config-file', '-f',
                            help='Specify an optional config file for the program.',
                            dest='configFile', default='default-config.yml')

        # Optional argument to specify logging directory.
        parser.add_argument('--log-dir', '-l',
                            help='Specify the directory where logs will be kept.',
                            dest='logDir')

        # Optional argument for specifying the data directory.
        parser.add_argument('--data-dir', '-d',
                            help='Specify the directory where data will be kept.',
                            dest='dataDir')

        # Flag to print version and exit.
        parser.add_argument('--version', '-v',
                            help='Print Version Info and Exit.',
                            action='store_true', dest='version')

        # Optionally Specify the encounter channel.
        parser.add_argument('--encounter-channel', '-e',
                            help='The channel for encounters to occur in.',
                            dest='encounterChannel')

        # Optionally Specify the presence.
        parser.add_argument('--presence', '-p',
                            help='What it looks like the bot is playing.',
                            dest='presence')

        # Optionally specify command prefix.
        parser.add_argument('--prefix', '-c',
                            help='The prefix to be used for commands.',
                            dest='prefix')

        # Specify Tenor API Key
        parser.add_argument('--tenor-api-key', '-t',
                            help='The API Key for Tenor.',
                            dest='tenorApiKey')

        # Filter out None keys.
        parsed = {k: v for k, v in vars(parser.parse_args()).items() if v is not None}

        # If the user asked for the version, print it and exit.
        if parsed['version']:
            print(f'Version: {Config.VERSION}')
            sys.exit()

        # Log and return the command line arguments
        self._log.debug(f'Command Line Arguments: {parsed}')
        return parsed

    def loadYaml(self, filename):
        """Return parsed yaml from given file."""
        self._log.debug(f"Parsing file arguments for file: '{filename}'...")

        # Open and parse the yaml from the file.
        with open(filename) as fd:
            parsed = yaml.safe_load(fd)

        # Log and return the arguments.
        self._log.debug(f'File Arguments ({filename}): {parsed}')
        return parsed
