"""Module that defines the discord bot client."""
import logging
import asyncio
import random
import re
import discord

import dj3k


class DJBot(discord.Client):
    """Subclass of Discord Client."""

    def __init__(self, config):
        """Initialize the DJBot."""
        self._log = logging.getLogger('bot.dj3k.DJBot')
        self.config = config
        self.redditor = dj3k.Redditor(self)
        self.scoreboard = dj3k.Scoreboard(self)
        self.encounter = False
        self.remarkLock = {}
        self.tenor = dj3k.Tenor(self.config.tenorApiKey)

        # Determine whether encounters are enabled based on whether encounterChannel is defined.
        self.config.encounterEnabled = getattr(self.config, 'encounterChannel', False) and True

        super().__init__()
        self._log.debug(f'Instantiated an instance of DJBot. ID: {id(self)}')

    def run(self):
        """Run the DJBot, using the token provided in the config file."""
        super().run(self.config.discordToken)

    async def on_ready(self):
        """Tasks to run when the client is connected and ready."""
        # Set the presence
        await self.setPresence(self.config.presence)

        # Log all the guilds we are members of.
        [self._log.info(f"We are a member of: {guild.name}") for guild in self.guilds]

        # If encounters are enabled, start the encounter timer.
        if self.config.encounterEnabled:
            self._log.info(f"Encounters will be sent in channel {self.config.encounterChannel}")
            await self.encounterTimer()
        else:
            self._log.info("Encounter channel not specified. Encounters disabled.")

    async def encounterTimer(self):
        """Kicks off the encounter timer based on the config in config."""
        channel = self.get_channel(self.config.encounterChannel)
        waitMin = self.config.encounter_times['time_between']['min']
        waitMax = self.config.encounter_times['time_between']['max']

        while True:
            # Wait for a random amount of time between waitMin and waitMax
            waitTime = random.randint(waitMin, waitMax)
            self._log.debug(f"Encounter wait, sleeping for {waitTime} seconds.")
            await asyncio.sleep(waitTime)

            # Spawn the actual encounter.
            await self.sendEncounter(channel)

    async def sendEncounter(self, channel):
        """Begins an encounter in the specified channel."""
        # If an encounter is already going, dont send a new one.
        if self.encounter:
            self._log.warning("An encounter is already happening, do not send a new one.")
            return

        # Prepare an encounter in the specified channel.
        self.encounter = dj3k.Encounter(self, channel)

        # Begin the encounter, this is a blocking call.
        await self.encounter.start()

        # When the encounter finishes, remove any references to it.
        self.encounter = None

    async def setPresence(self, newPresence):
        """Set the presence of this bot."""
        self._log.info(f'Changed presence to: {newPresence}.')
        activity = discord.Game(name=newPresence)
        await self.change_presence(activity=activity)

    async def on_message(self, msg):
        """Tasks to be run whenever a message is sent."""
        # If we sent the message, ignore it.
        if msg.author == self.user:
            self._log.debug("Ignoring our own message")
            return

        # Set which functions will handle which commands.
        commandHandlers = {
            self.config.prefix: self.cmdMeme,
            self.config.prefix + ' moo': self.cmdMoo,
            self.config.prefix + ' meme': self.cmdMeme,
            self.config.prefix + ' encounter': self.cmdEncounter,
            self.config.prefix + ' catch': self.cmdCatch,
            self.config.prefix + ' clean channel': self.cmdCleanChannel,
            self.config.prefix + ' moo_irl': self.cmdMooIrl,
            self.config.prefix + ' score': self.cmdScore,
            self.config.prefix + ' version': self.cmdVersion,
            'fuck off dj': self.cmdFuckOff
        }

        # Try to run the handler for the given command
        try:
            await commandHandlers[msg.content](msg)
        except KeyError:
            # If there is no handler...
            # ...try responding to the message as if it was a subreddit.
            await self.trySubreddit(msg)

            # ...try responding to the message as if it is a remark.
            await self.tryRemark(msg)

    async def trySubreddit(self, msg):
        """Post a link to the subreddit if the message references a subreddit."""
        # Check the message against the subreddit regular expression.
        match = re.search(r'^(?!http[s]?).*\/?r\/([a-zA-Z0-9_]{3,21})', msg.content.lower())

        # If there was no match, return.
        if match is None:
            return

        # Get the name of the subreddit from the match.
        sub_name = match[1].title()

        # If the subreddit is valid and exists, post a link to it.
        if await self.redditor.is_subreddit(sub_name):
            embed = discord.Embed()
            embed.title = sub_name
            embed.description = f"Here is a link to {sub_name}"
            embed.type = 'rich'
            embed.url = f"https://reddit.com/r/{sub_name}/"

            await msg.channel.send(embed=embed)

    async def tryRemark(self, msg):
        """Reply if there is a remark configured for this message."""
        # Roll a die, to see if a remark will evenbe attempted.
        randNum = random.random()
        if randNum <= self.config.remarkChance:
            self._log.debug(f'Remark in {msg.channel.name} skipped due to chance '
                            f'({randNum} <= {self.config.remarkChance}).')
            return
        else:
            self._log.debug(f'Will send remark in {msg.channel.name} '
                            f'due to chance ({randNum} <= {self.config.remarkChance}).')

        # If that particular channel is locked, ignore the message.
        # since its possible a message has not been sent in that channel before,
        # the channel.id wouldn't be in the dict and would throw a KeyError. In that
        # case, add the key and set it to unlocked. Then lock it anyway.
        try:
            if self.remarkLock[msg.channel.id]:
                return
        except KeyError:
            self.remarkLock[msg.channel.id] = False

        for remark in self.config.remarks:
            # If the message is a valid remark...
            if msg.content == remark['q']:
                # ...send a random response from the remark's list of responses.
                await msg.channel.send(content=random.choice(remark['a']))
                # Lock sending another remark
                await self.lockRemarks(msg.channel)

    async def lockRemarks(self, channel):
        """Locks sending remarks in a `channel` for a random range of seconds."""
        # Determine a string to represent the channel.
        chanID = channel.id

        # If that particular channel is locked, ignore it.
        if self.remarkLock[chanID]:
            return

        # Pick a random number to lock remarks for.
        duration = random.randint(self.config.remarkTimeout['min'],
                                  self.config.remarkTimeout['max'])

        # Lock remarks, wait `duration` seconds, then unlock remarks.
        self.remarkLock[chanID] = True
        self._log.info(f'Remarks are now locked in channel {chanID} for {duration} seconds.')
        await asyncio.sleep(duration)
        self.remarkLock[chanID] = False
        self._log.info(f'Remarks have been unlocked in channel {chanID}.')

    def isUserAdmin(self, user):
        """Return boolean about if the user is configured as an admin."""
        return user.id in self.config.admins

    async def cmdVersion(self, msg):
        """Reply with the version as defined in the config file."""
        content  = f'Verison: {dj3k.Config.VERSION}.\n'
        content += f'Environment: {self.config.environment}.\n'
        content += f'Started At: {self.config.startTime}.'
        await msg.channel.send(content=content)

    async def cmdMoo(self, msg):
        """Handle Moo command."""
        await msg.channel.send(content=":cow: Moo Moo to you too. :cow2: ")

    async def cmdMeme(self, msg):
        """Handle Meme Command."""
        async with msg.channel.typing():
            embed = await self.redditor.getRandomMemeEmbed()
            await msg.channel.send(embed=embed)

    async def cmdEncounter(self, msg):
        """Handle Encounter Command."""
        self._log.warning(f"{msg.author.name} wants to summon an "
                          f"encounter in {msg.guild.name}/{msg.channel.name}.")

        # If the user is not an admin, log an error.
        if not self.isUserAdmin(msg.author):
            self._log.error(f"{msg.author.name} is not permitted to summon an encounter.")
            await msg.channel.send('You are not allowed to do that.')
        elif not self.config.encounterEnabled:
            self._log.error(f"Encounters are currently disabled.")
            await msg.channel.send('Encounters are disabled.')
        else:
            self._log.info(f"{msg.author.name} has summoned an encounter in {msg.channel}.")
            await self.sendEncounter(msg.channel)

    async def cmdCatch(self, msg):
        """Handle Catch command."""
        # If an encounter is currently happending, and we are in the encounter channel...
        if self.encounter and (msg.channel == self.encounter.channel):
            # ...attempt a catch.
            await self.encounter.attemptCatch(msg)
        else:
            # ...otherwise, there is nothing to catch.
            self._log.info("Tried to catch, but there is no encounter applicable.")
            await msg.channel.send("There is nothing to catch, you silly human :smirk: ")

    async def cmdCleanChannel(self, msg):
        """Handle Clean Channel command."""
        self._log.info(f"{msg.author.name} Wants to Clean the Channel {msg.guild.name}/{msg.channel.name}")

        # Verify the requestor is an admin and can do this.
        if not self.isUserAdmin(msg.author):
            self._log.error("{msg.author.name} is not permitted to do this.")
        else:
            # If permitted, perform the purge.
            self._log.info(f"Removing Messages in channel {msg.guild.name}/{msg.channel.name}")
            await msg.channel.purge(limit=1000)

    async def cmdMooIrl(self, msg):
        """Handle Moo Irl Command."""
        async with msg.channel.typing():
            # Post a random meme from moo_irl
            embed = await self.redditor.getRandomMemeEmbed(search_subs=['moo_irl'])
            await msg.channel.send(embed=embed)

    async def cmdScore(self, msg):
        """Handle Score command."""
        async with msg.channel.typing():
            # Post the score if the requestor.
            score = self.scoreboard.getScore(msg.author.id)
            await msg.channel.send(content=f"{msg.author.mention}, you currently have a score of {score}.")

    async def cmdFuckOff(self, msg):
        """Apologize and leave.

        Bot performs the following actions:
        1. Posts a sad reaction gif.
        2. Says random sorry message.
        3. Deletes it's last 100 messages in the channel.
        4. Locks remarks in the channel.

        If remarks are already locked in the channel, do nothing."""
        try:
            if self.remarkLock[msg.channel.id]:
                return
        except KeyError:
            self.remarkLock[msg.channel.id] = False

        self._log.info(f'Fucking off in channel {msg.channel.name}')

        async with msg.channel.typing():
            sadGif = self.tenor.randomGifSearch('sad')

        gifUrl = sadGif['media'][0]['gif']['url']
        apology = random.choice(self.config.apologies)

        # Send the sad gif
        await msg.channel.send(gifUrl)
        # Wait a few seconds
        await asyncio.sleep(3)
        # Send the apology
        await msg.channel.send(apology)
        # Wait a few more seconds
        await asyncio.sleep(6)

        # Delete the last 100 of this bot's messages in the given channel
        await msg.channel.purge(check=lambda m: m.author == self.user)

        # Lock remarks in this channel
        await self.lockRemarks(msg.channel)
