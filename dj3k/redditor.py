"""Module for reddit client object."""
import logging
import random
import praw
import discord

from prawcore.exceptions import NotFound


class Redditor(praw.Reddit):
    """Defines a reddit client."""

    def __init__(self, djbot):
        """Initialize the object."""
        self._log = logging.getLogger('bot.dj3k.Redditor')
        self.myConfig = djbot.config

        # Login to the client, running the parent class's init method
        super().__init__(client_id=self.myConfig.redditClientID,
                         client_secret=self.myConfig.redditClientSecret,
                         user_agent="DJ 3000")

    async def getRandomMemeEmbed(self, search_subs=None):
        """Get a random meme from a randomly selected sub in a list of subs."""
        # If no subs passed explicitly, use the defaults.
        if search_subs is None:
            search_subs = self.myConfig.memeSubs

        # Select a random sub from the sub list
        sub = random.choice(search_subs)

        # Get a list of the top 100 non self posts from the sub
        posts = [post for post in self.subreddit(sub).hot(limit=100) if not post.is_self]

        # Select a random post a the meme to send
        if posts:
            post = random.choice(posts)
        else:
            # TODO Handle when no self posts were found in the top 100 hot
            pass

        self._log.info(f"Grabbed meme from '/r/{sub}' titled '{post.title}'")

        # Create and configure the embed to send
        embed = discord.Embed()
        embed.title = post.title
        embed.type = 'rich'
        embed.description = f"Special Thanks to /r/{sub}"
        embed.url = post.shortlink
        embed.set_image(url=post.url)

        # Return the embed
        return embed

    async def is_subreddit(self, subreddit):
        """Check if a certain subreddit exists."""
        try:
            self.subreddits.search_by_name(subreddit, exact=True)
        except NotFound:
            return False
        else:
            return True
