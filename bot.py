"""Initial run script for program."""
import os
import time
import logging
import logging.handlers

import dj3k


def createDir(directory):
    """Create the specified directory if it doesn't already exist."""
    try:
        os.makedirs(directory)
    except FileExistsError:
        pass


# Get the configuration options
config = dj3k.Config().opts

# Log the time this script started.
config.startTime = time.strftime('%Y-%m-%d %H:%M:%S')

# Create the Log and Data directories
createDir(config.logDir)
createDir(config.dataDir)

# Configure the logger
logFmt = logging.Formatter('%(asctime)s [%(levelname)s] (%(name)s): %(message)s')

# Setup the console handler
consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(logFmt)

# Setup the File handler, this automatically creates a new file daily.
fileHandler = logging.handlers.TimedRotatingFileHandler(config.logDir + 'dj3k.log',
                                                        when='midnight')
fileHandler.setFormatter(logFmt)

# Get the logger
log = logging.getLogger('bot')
log.addHandler(consoleHandler)
log.addHandler(fileHandler)
log.setLevel(int(config.logLevel))

# Instantiate the bot with the config, and run it.
bot = dj3k.DJBot(config)
bot.run()
